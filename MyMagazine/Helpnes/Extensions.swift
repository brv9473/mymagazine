//
//  SnapshotExtensions.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 05.12.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation
import FirebaseFirestore
import UIKit

fileprivate var aView: UIView?

extension QueryDocumentSnapshot {
    
    func decoded<T: Decodable>() throws -> T {
        let jsonData = try JSONSerialization.data(withJSONObject: data(), options: [])
        let object = try JSONDecoder().decode(T.self, from: jsonData)
        
        return object
    }
}


extension QuerySnapshot {
    
    func decoded<Type: Decodable>() throws -> [Type] {
        let objects: [Type] = try documents.map({ try $0.decoded() })
        return objects
    }
}

extension UIViewController {
    
    func showSpinner() {
        aView = UIView(frame: self.view.bounds)
        aView?.backgroundColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let activityIndecator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndecator.center = aView!.center
        activityIndecator.startAnimating()
        aView?.addSubview(activityIndecator)
        self.view.addSubview(aView!)
    }
    
    
    func hideSpinner() {
        aView?.removeFromSuperview()
        aView = nil
    }
}


