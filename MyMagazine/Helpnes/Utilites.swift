//
//  Utilites.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 03.12.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class Utilites {
    
//    static func styleTextField(_ textfield: UITextField) {
//
//        //Create the button line
//        let buttonLine = CALayer()
//
//        buttonLine.frame = CGRect(x: 0, y: textfield.frame.height - 1, width: textfield.frame.width, height: 2)
//
//        buttonLine.backgroundColor = UIColor.black.cgColor
//        
//        //Remove border on text field
//        textfield.borderStyle = .none
//
//        //Add the line to the text field4
//        textfield.layer.addSublayer(buttonLine)
//
//        textfield.layer.masksToBounds = true
//    }
    
    static func styleFillButton(_ button: UIButton) {
        
            //Filled rounder corner style
        button.backgroundColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1)
        button.layer.cornerRadius = 25.0
        button.tintColor = .white
    }
    
    static func styleHollowButton(_ button: UIButton) {
        
        //Hollow rounder corner style
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 25.0
        button.tintColor = .black
    }
    
    static func isPasswordValid (_ password: String) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        
        return passwordTest.evaluate(with: password)
    }
}
