//
//  CategoryModel.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 22.10.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation

struct Category {
    
    // MARK: - Properties

    let name: String
}
