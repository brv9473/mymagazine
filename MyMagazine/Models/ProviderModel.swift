//
//  ProviderModel.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 22.10.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation

struct Provider {
    
    // MARK: - Properties

    let name: String
    let phone: String
    let eMail: String
    
}
