//
//  UserModel.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 22.10.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation


struct User: Codable {
    
    // MARK: - Properties

    let firstname: String
    let lastname: String
    let email: String
    let uid: String
}
