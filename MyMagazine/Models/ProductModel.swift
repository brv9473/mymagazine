//
//  ProductModel.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 22.10.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation



struct Product {
    
    // MARK: - Types

    enum Size: String {
        case xxs, xs, s, m, l, xl, xxl, xxxl
    }
    
    // MARK: - Properties

    let name: String
    let code: String
    let sizeOfItem: Size
    let color: String
    let buyPrice: Int
    let minSellPrice: Int
    let maxSellPrixe: Int
    let countOfItems: Int
    let images: [String]

}
