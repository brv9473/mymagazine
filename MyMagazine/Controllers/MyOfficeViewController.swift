//
//  MyOfficeViewController.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 29.11.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class MyOfficeViewController: ViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // MARK: - IBOutlets
    
    @IBOutlet weak var accImageView: AccountImageView!
    @IBOutlet weak var nameView: CastomView!
    @IBOutlet weak var secondNameView: CastomView!
    @IBOutlet weak var phoneView: CastomView!
    @IBOutlet weak var emailView: CastomView!
    @IBOutlet weak var passwordView: CastomView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var savingInfoButton: UIButton!
    

    // MARK: - Properties

    var imagePicker = UIImagePickerController()
    var scrollView = UIScrollView()
    let db = Firestore.firestore()
    lazy var userCollections = Firestore.firestore().collection("users")
 
    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //selectors
        savingInfoButton.addTarget(self, action: #selector(savingUserInfo), for: .touchUpInside)
        exitButton.addTarget(self, action: #selector(logOutWhenTapped), for: .touchUpInside)
        
        // Adding tapGesture on accontImage
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedMe))
        accImageView.addGestureRecognizer(tap)
        accImageView.isUserInteractionEnabled = true
        tap.isEnabled = false
        
        // ScrollView settings
        scrollView.contentSize = self.view.bounds.size
        scrollView.autoresizingMask = UIView.AutoresizingMask.flexibleWidth
        scrollView.autoresizingMask = UIView.AutoresizingMask.flexibleHeight
        self.view.addSubview(scrollView)
        
        getDataFromFirebase()
        setupForCastomViews()
    }
    
    
    // MARK: - Functions
    
    @IBAction func editingInfoButton(_ sender: Any) {
        nameView.textField.isEnabled = true
        secondNameView.textField.isEnabled = true
        phoneView.textField.isEnabled = true
        emailView.textField.isEnabled = true
        passwordView.textField.isEnabled = true
        
        UIView.animate(withDuration: 0.3) {
            self.savingInfoButton.alpha = 1
        }
    }
    
    @objc func savingUserInfo() {
        nameView.textField.isEnabled = false
        secondNameView.textField.isEnabled = false
        phoneView.textField.isEnabled = false
        emailView.textField.isEnabled = false
        passwordView.textField.isEnabled = false
        
        UIView.animate(withDuration: 0.3) {
            self.savingInfoButton.alpha = 0
        }
        
        
        
    }
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "No camera find", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGellery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "No Library find", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            accImageView.accImage.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func tappedMe() {
        let alert = UIAlertController(title: "Chose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
            self.openGellery()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupForCastomViews() {
        nameView.label.text = "Ім'я"
        secondNameView.label.text = "Прізвище"
        phoneView.label.text = "Телефон"
        emailView.label.text = "Email"
        passwordView.label.text = "Пароль"
        
        passwordView.textField.textContentType = .password
        passwordView.textField.isSecureTextEntry = true

        Utilites.styleHollowButton(savingInfoButton)
        
        Utilites.styleHollowButton(exitButton)
        
        nameView.textField.isEnabled = false
        secondNameView.textField.isEnabled = false
        phoneView.textField.isEnabled = false
        emailView.textField.isEnabled = false
        passwordView.textField.isEnabled = false

    }
    
    func getDataFromFirebase() {
        //Get users info from FireBase
        
        addSpinner()
        userCollections.getObjects(of: User.self) { (users) in
            let currentUSer = Auth.auth().currentUser
            for user in users! {
                if user.uid == currentUSer?.uid {
                    self.nameView.textField.text = user.firstname
                    self.secondNameView.textField.text = user.lastname
                    self.phoneView.textField.text = ""
                    self.emailView.textField.text = user.email
                    self.passwordView.textField.text = ""
                    print(user.uid)
                }
            }
        }
    }
    
    func addSpinner() {
        self.showSpinner()

        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            self.hideSpinner()
        }
    }

    
    @objc func logOutWhenTapped() {
        do {
            try Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            navigationController?.pushViewController(vc, animated: true)
            print("User logget out")
        } catch let error {
            print(error)
        }
    }
}


extension CollectionReference {
    func getObjects<T: Decodable>(of type: T.Type, completion: @escaping ([T]?) -> Void) {
        getDocuments { (snapshot, _) in
            do {
                completion(try snapshot!.decoded())
                
            } catch {
                print(error.localizedDescription)
                completion(nil)
            }
        }
    }
    
    
    func addObject(_ object: Encodable, completion: ((Error?) -> Void)? = nil) {

    }
}



