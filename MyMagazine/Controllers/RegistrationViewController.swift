//
//  RegistrationViewController.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 03.12.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class RegistrationViewController: ViewController, UITextFieldDelegate {

    // MARK: - IBOutlets

    @IBOutlet weak var firstNameView: CastomView!
    @IBOutlet weak var lastNameView: CastomView!
    @IBOutlet weak var emailView: CastomView!
    @IBOutlet weak var passwordView: CastomView!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstNameView.textField.delegate = self
        lastNameView.textField.delegate = self
        emailView.textField.delegate = self
        passwordView.textField.delegate = self
    
}
    
    override func viewDidLayoutSubviews() {
        setUpElements()
    }
    
    // MARK: - Functions
    
    func setUpElements() {
        
        //Hide the error label
        errorLabel.alpha = 0
        
        //Style the elements
        Utilites.styleHollowButton(signUpButton)
        emailView.textField.keyboardType = .emailAddress
        passwordView.textField.textContentType = .password
        
        firstNameView.label.text = "Ім'я"
        lastNameView.label.text = "Прізвище"
        emailView.label.text = "Email"
        passwordView.label.text = "Пароль"
        
    }
    
    
    //Chek the fields and if everything is correct, this method returns nil. Otherwise, it returns error message
    func valideteFields() -> String? {
        
        //Check all fields are filled in
        if firstNameView.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastNameView.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailView.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordView.textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Будь ласка заповніть всі лінійки"
        }
        
        //Check the password
        let cleanPassword = passwordView.textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilites.isPasswordValid(cleanPassword) == false {
            //Password isn`t source enough
            return "Будь ласка введіть пароль НАРМАЛЬНА"
        }
        
        return nil
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        
        //Validate the fields
        let error = valideteFields()
        
        if error != nil {
            //Something wrong with the fields, show error message
            showError(error!)
        } else {
            //Create cleand versions of the data
            let firstname = firstNameView.textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastname = lastNameView.textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailView.textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordView.textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            //Create the user
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                //Check for errors
                if err != nil {
                    //There was an error creating the user
                    self.showError("Error creating user")
                } else {
                    //User was created successfully, niw store the firs name and last name
                    let db = Firestore.firestore()
                    db.collection("users").addDocument(data: ["firstname": firstname,
                                                              "lastname": lastname,
                                                              "email": email,
                                                              "password": password,
                                                              "uid": result!.user.uid
                    ]) { (error) in
                        if error != nil {
                            self.showError("Error saving user data")
                        }
                    }
                    //Transition to the home screen
                    self.transitionToHome()
                }
            }
        }
    }
    
    func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    func transitionToHome() {
                let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController
                navigationController?.pushViewController(vc!, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        firstNameView.textField.resignFirstResponder()
        lastNameView.textField.resignFirstResponder()
        emailView.textField.resignFirstResponder()
        passwordView.textField.resignFirstResponder()
        return true
    }
}
