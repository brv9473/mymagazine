//
//  LoginViewController.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 27.11.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var nameView: CastomView!
    @IBOutlet weak var passWorodView: CastomView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    // MARK: - Properties
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Selectors
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        loginButton.addTarget(self, action: #selector(loginButtonSiTapped), for: .touchUpInside)
        
        
        nameView.textField.delegate = self
        passWorodView.textField.delegate = self
        
        self.view.backgroundColor = .black
        
        view.addGestureRecognizer(tap)
        hideKeyboardWhenTappedAround()
        loginViewsSetup()
    }
    
    // MARK: - Functions
    func loginViewsSetup() {
        
        // Setap castom views for login
        nameView.label.text = "email"
        
        passWorodView.label.text = "Пароль"
        passWorodView.textField.textContentType = .password
        passWorodView.textField.isSecureTextEntry = true
        
        imageView.image = UIImage(named: "loginImage")
        
        
        //Corner for backgroundView
        backGroundView.clipsToBounds = true
        backGroundView.layer.cornerRadius = 30
        backGroundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        //Buttons style
        Utilites.styleHollowButton(loginButton)
        Utilites.styleHollowButton(registerButton)
        
        nameView.textField.text! = "qwe@gmail.com"
        passWorodView.textField.text = "Bodnrost123*"
    }
    
    //Show/Hidden keyboard
    @objc func keyboardWillChange(notification: Notification) {
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            view.transform.ty = -keyboardRect.height
        } else {
            view.transform.ty = 0
        }
    }
    
    //Hidden keyboar when tapping Enter on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nameView.textField.resignFirstResponder()
        passWorodView.textField.resignFirstResponder()
        return true
    }
    
    //Hidden keybord when tapping anywhere
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func loginButtonSiTapped() {
        showSpinner()
        
        //Create cleaned versions of the text
        let email = nameView.textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passWorodView.textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //Signin in the user
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                //Couldn`t sign in
                let alert = UIAlertController(title: "Помилка", message: "Неправильно ввелений email або пароль", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let user = Auth.auth().currentUser
                if user == user {
                    self.hideSpinner()
                    let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController
                    UIApplication.shared.keyWindow?.rootViewController = vc
                    UIApplication.shared.keyWindow?.makeKeyAndVisible()
                }
            }
        }
    }
}
