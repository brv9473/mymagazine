//
//  HomeViewController.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 28.11.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class HomeViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
