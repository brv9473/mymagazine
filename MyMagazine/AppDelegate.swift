//
//  AppDelegate.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 22.10.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        return true
    }
}

