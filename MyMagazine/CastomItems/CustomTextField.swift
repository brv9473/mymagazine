//
//  CustomTextField.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 04.12.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        let  buttonLine = UIView()
        buttonLine.backgroundColor = .black
        self.borderStyle = .none
        
        buttonLine.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(buttonLine)
        
        NSLayoutConstraint.activate([
            buttonLine.leftAnchor.constraint(equalTo: self.leftAnchor),
            buttonLine.rightAnchor.constraint(equalTo: self.rightAnchor),
            buttonLine.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            buttonLine.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
    
}
