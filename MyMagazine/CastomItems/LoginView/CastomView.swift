//
//  CastomView.swift
//  MyMagazine
//
//  Created by Rostyslav Bodnar on 27.11.2019.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class CastomView: UIView {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: CustomTextField!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    
    func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder), owner: self, options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
    }
}
